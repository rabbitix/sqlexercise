Ex 4.3:
1:
SELECT S.sname
FROM	Suppliers S, Parts P, Catalog C
WHERE	P.color=’red’ AND C.pid=P.pid AND C.sid=S.sid ;
2:
SELECT C.sid
FROM	Catalog C, Parts P
WHERE	(P.color = ‘red’ OR P.color = ‘green’)
AND P.pid = C.pid ;
3:
SELECT S.sid
FROM	Suppliers S
WHERE	S.address = ‘221 Packer street’
OR S.sid IN ( SELECT C.sid
                FROM	Parts P, Catalog C
                WHERE	P.color=’red’ AND P.pid = C.pid ) ;
4:
SELECT C.sid
FROM	Parts P, Catalog C
WHERE	P.color = ‘red’ AND P.pid = C.pid
AND EXISTS ( SELECT P2.pid
            FROM	Parts P2, Catalog C2
            WHERE	P2.color = ‘green’ AND C2.sid = C.sid
            AND  P2.pid = C2.pid ) ;
5:
SELECT C.sid
FROM	Catalog C
WHERE	NOT EXISTS (SELECT P.pid
              FROM	Parts P
              WHERE	NOT  EXISTS  (SELECT C1.sid
                                  FROM	Catalog C1
                                  WHERE	C1.sid = C.sid
                                  AND C1.pid = P.pid)) ;

6:
SELECT C.sid
FROM	Catalog C
WHERE	NOT EXISTS (SELECT P.pid
                FROM	Parts P
                WHERE	P.color = ‘red’
                AND  (NOT  EXISTS  (SELECT C1.sid
                                    FROM	Catalog C1
                                    WHERE	C1.sid = C.sid AND
                                    C1.pid = P.pid))) ;
7:
SELECT C.sid
FROM	Catalog C
WHERE	NOT EXISTS (SELECT P.pid
                  FROM	Parts P
                  WHERE	(P.color = ‘red’ OR P.color = ‘green’)
                  AND  (NOT  EXISTS  (SELECT C1.sid
                                      FROM	Catalog C1
                                      WHERE	C1.sid = C.sid AND
                                      C1.pid = P.pid))) ;
8:
SELECT C.sid
FROM	Catalog C
WHERE	(NOT EXISTS (SELECT P.pid
                    FROM	Parts P
                    WHERE	P.color = ‘red’ AND
                    (NOT  EXISTS  (SELECT  C1.sid
                                    FROM	Catalog C1
                                    WHERE	C1.sid = C.sid AND
                                    C1.pid = P.pid))))
                                    OR ( NOT EXISTS (SELECT P1.pid
                                                    FROM	Parts P1
                                                    WHERE	P1.color = ‘green’ AND
                                                    (NOT  EXISTS  (SELECT  C2.sid
                                                                    FROM	Catalog C2
                                                                    WHERE	C2.sid = C.sid AND
                                                                    C2.pid = P1.pid)))) ;


9:
SELECT C1.sid, C2.sid
FROM	Catalog C1, Catalog C2
WHERE	C1.pid = C2.pid AND C1.sid = C2.sid
AND C1.cost > C2.cost ;

10:
SELECT C.pid FROM	Catalog C
WHERE	EXISTS (SELECT C1.sid
FROM	Catalog C1
WHERE	C1.pid = C.pid AND C1.sid ƒ= C.sid ) ;

11:
SELECT C.pid
FROM	Catalog C, Suppliers S
WHERE	S.sname = ‘Yosemite Sham’ AND C.sid = S.sid
AND C.cost	ALL (Select C2.cost
                  FROM   Catalog C2, Suppliers S2
                  WHERE S2.sname = ‘Yosemite Sham’
                  AND  C2.sid = S2.sid) ;

================================================
Ex 4.5:
1:
SELECT C.eid
FROM	Aircraft A, Certiﬁed C
WHERE	A.aid = C.aid AND A.aname = ‘Boeing’ ;
2:
SELECT E.ename
FROM	Aircraft A, Certiﬁed C, Employees E
WHERE	A.aid = C.aid AND A.aname = ‘Boeing’ AND E.eid = C.eid ;
3:
SELECT A.aid
FROM	Aircraft A, Flights F
WHERE	F.from = ‘Bonn’ AND F.to = ‘Madrid’ AND
A.cruisingrange > F.distance ;
4:
SELECT E.ename
FROM	Aircraft A, Certiﬁed C, Employees E, Flights F
WHERE	A.aid = C.aid AND E.eid = C.eid AND
distance < cruisingrange AND salary > 100,000 ;

5:
SELECT E.ename
FROM Certiﬁed C, Employees E, Aircraft A
WHERE A.aid = C.aid AND E.eid = C.eid AND A.cruisingrange > 3000
AND E.eid NOT IN ( SELECT C2.eid
                    FROM Certiﬁed C2, Aircraft A2
                    WHERE C2.aid = A2.aid AND A2.aname = ‘Boeing’ ) ;
6:
SELECT E.eid
FROM	Employees E
WHERE	E.salary = ( Select MAX (E2.salary)
                  FROM Employees E2 ) ;
7:
SELECT E.eid
FROM Employees E
WHERE E.salary = (SELECT MAX (E2.salary)
                  FROM	Employees E2
                  WHERE	E2.salary = (SELECT MAX (E3.salary)
                                      FROM	Employees E3 )) ;
8:
SELECT Temp.eid
FROM	( SELECT	C.eid AS eid, COUNT (C.aid) AS cnt,
        FROM	Certiﬁed C
        GROUP BY C.eid) AS Temp
        WHERE	Temp.cnt = ( SELECT	MAX (Temp.cnt)
                            FROM	Temp) ;

9:
SELECT C1.eid
FROM	Certiﬁed C1, Certiﬁed C2, Certiﬁed C3
WHERE	(C1.eid = C2.eid AND C2.eid = C3.eid AND
C1.aid ƒ= C2.aid AND  C2.aid ƒ= C3.aid AND  C3.aid =ƒ	C1.aid)
EXCEPT SELECT C4.eid
        FROM	Certiﬁed C4, Certiﬁed C5, Certiﬁed C6, Certiﬁed C7,
        WHERE (C4.eid = C5.eid AND C5.eid = C6.eid AND
              C6.eid = C7.eid AND C4.aid ƒ= C5.aid AND
              C4.aid ƒ= C6.aid AND C4.aid ƒ= C7.aid AND
              C5.aid ƒ= C6.aid AND C5.aid ƒ= C7.aid AND C6.aid ƒ= C7.aid ) ;

10:
SELECT SUM (E.salaries)
FROM	Employees E ;



